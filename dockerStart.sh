#! /bin/bash

if [ $# -ne 2 ];then
  echo "Usage: $0 <CONTAINER_NAME> <IMAGE_ID>"
 sudo docker images
  echo "running containers"
 sudo docker ps -a
  exit 1
fi

#HOST_NETWORKINTERFACE=wlan0
HOST_NETWORKINTERFACE=enp0s25
HOST_IP=$(/sbin/ifconfig $HOST_NETWORKINTERFACE |  grep -Ei 'inet Adres*' | cut -d: -f2 | awk '{ print $1}')
echo "HOSTIP $HOST_IP"

LXC_NAME=$1
IMAGE_ID=$2



if [ -e /tmp/$LXC_NAME.cid ];then
  yes |  rm /tmp/$LXC_NAME.cid 
fi  

#MOUNT=" -v /sys/fs/cgroup:/sys/fs/cgroup:ro    -v /opt/apache-maven:/opt/apache-maven:ro   -v /home/andes/tmp:/shared:rw  -v /etc/hosts:/etc/hosts:rw "
MOUNT="   -v /home/andreas/tmp:/shared:rw  -v /etc/hosts:/etc/hosts:rw "
#MOUNT=" -v /sys/fs/cgroup:/sys/fs/cgroup:ro       -v /home/andi/tmp:/shared:rw  -v /etc/hosts:/etc/hosts:rw "
DOCKER_OPTIONS=" -h=$LXC_NAME   --cidfile=/tmp/$LXC_NAME.cid  --name=$LXC_NAME    -e VIRTUAL_HOST=$LXC_NAME   $MOUNT    $IMAGE_ID    /bin/bash"
#LXC_CONF="--lxc-conf=\"lxc.network.ipv4 =  :172.17.0.5\""
LXC_CONF=""




startD(){

#Daemon  ohne  Portforwarding
sudo   docker run  --privileged=false    -d -ti  $DOCKER_OPTIONS 
#log

#########################################################################################################################################
##Forwadring von 8080 auf den HOST
#sudo   docker run  --privileged=false  --rm    -ti     -p $HOST_IP:8080:8080    $DOCKER_OPTIONS)


#########################################################################################################################################

##Ohne Protforwarding auf den HOST
#sudo   docker run  --privileged=false  --rm    -ti $DOCKER_OPTIONS

#########################################################################################################################################

#Daemon  mit Portforwarding
#id=$(sudo   docker run  --privileged=false    -d -ti   -p $HOST_IP:8080:8080 $DOCKER_OPTIONS  )
#id=$(sudo   docker run  --privileged=false    -d -ti   -p 8080:8080 $DOCKER_OPTIONS  )
#log()

#########################################################################################################################################

}


startTTy(){

#Daemon  ohne  Portforwarding
#sudo   docker run  --privileged=false    -d  $DOCKER_OPTIONS 
#log

#########################################################################################################################################
##Forwadring von 8080 auf den HOST
#sudo   docker run  --privileged=false  --rm    -ti     -p $HOST_IP:8080:8080    $DOCKER_OPTIONS)


#########################################################################################################################################

##Ohne Protforwarding auf den HOST
sudo   docker run  --privileged=false  --rm    -ti $DOCKER_OPTIONS

#########################################################################################################################################

#Daemon  mit Portforwarding
#id=$(sudo   docker run  --privileged=false    -d -ti   -p $HOST_IP:8080:8080 $DOCKER_OPTIONS  )
#id=$(sudo   docker run  --privileged=false    -d -ti   -p 8080:8080 $DOCKER_OPTIONS  )
#log()

#########################################################################################################################################

}


stopLast(){
  sudo   docker ps -q | xargs sudo docker stop
}



stopAll(){
  sudo   docker ps -aq | xargs sudo docker stop
}


rmAll(){
  sudo   docker ps -aq | xargs sudo docker stop
  sudo   docker ps -aq | xargs sudo docker rm
}

listImages(){
  sudo docker images 
}

#Show image dependencies and save as PNG Image
printImageDeps(){
    echo "printing images dependencies to $(pwd)/docker-dependencies.png"
    sudo docker images -viz | dot -Tpng -o docker.png
}


log () {
    # -p :$port will establish a mapping of 12345->12345 from outside docker to
    # inside of docker.
    # Then, the application must observe the PORT environment variable
    # to launch itself on that port; This is set by -e PORT=$port.

    # Additional goodies:
    CONTAINERID=$(sudo docker ps -lq) # <-- the running id of your container
    DOCKER_IP=$(sudo docker inspect --format="{{ .NetworkSettings.IPAddress }}" $CONTAINERID)
    echo "DOCKER_IP: $DOCKER_IP"

    #echo $id > /app/files/CONTAINER # <-- remember Docker id for this instance
    docker ps # <-- check that the app is running
    docker logs $CONTAINERID# <-- look at the output of the running instance
    #docker kill $id # <-- to kill the app

  
    #sudo sed -i "3i $IP $LXC_NAME" /etc/hosts

}

startTTy
#startD
#log
