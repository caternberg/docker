#!/bin/bash
### BEGIN INIT INFO
# Provides:          tomee
# Required-Start:    $local_fs $remote_fs $network
# Required-Stop:     $local_fs $remote_fs $network
# Should-Start:      $named
# Should-Stop:       $named
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: TomEE_Plus_ic
# Description:       start TomEE with iC
### END INIT INFO
 
#TomEE auto-start
#description: Auto-starts TomEE
#processname: tomee
#pidfile: /var/run/tomee.pid
 
export JAVA_HOME=/usr/lib/jvm/java-7-openjdk-amd64
TOMEE_HOME=/usr/local/tomee
RUN_AS=tomee
case $1 in
 
start)
  /bin/su $RUN_AS -c $TOMEE_HOME/bin/startup.sh
  ;;
 
stop)  
  /bin/su $RUN_AS -c $TOMEE_HOME/bin/shutdown.sh
  ;;
 
restart)
  /bin/su $RUN_AS -c $TOMEE_HOME/bin/shutdown.sh
  /bin/su $RUN_AS -c $TOMEE_HOME/bin/startup.sh
  ;;
esac
 
exit 0