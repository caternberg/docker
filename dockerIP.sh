#! /bin/bash



docker run \
-n=false \
--net="bridge" \
--lxc-conf="lxc.network.type = veth" \
--lxc-conf="lxc.network.ipv4 = 172.17.0.10" \
--lxc-conf="lxc.network.ipv4.gateway = 172.17.42.1" \
--lxc-conf="lxc.network.link = docker0" \
--lxc-conf="lxc.network.name = docker0" \
--lxc-conf="lxc.network.flags = up" \
-i -t $1 /bin/bash